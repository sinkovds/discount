<?php
//клвасс скидки
class Discount {

    private $type = 0; // тип скидки 0 - скидка задана множеством продуктов, 1 - скидка на количество продуктов
    private $productNames;

    private $productCount = 0;
    private $excludedProducts = array();

    private $discount;

    public function Discount($productNames, $discount, $type = 0, $productCount = 0, $excludedProducts = array()){
        $this->type = 0;
        $this->productNames = $productNames;
        $this->discount = $discount;
        $this->type = $type;
        $this->productCount = $productCount;
        $this->excludedProducts = $excludedProducts;

    }

    public static function create($productNames, $discount){
        return new Discount($productNames, $discount);
    }

    public static function createByCount($productCount,$discount, $excludedProducts){
        return new Discount(null, $discount, 1, $productCount, $excludedProducts);
    }

    public function getExcludedProducts(){
        return $this->excludedProducts;
    }

    public function getType(){
        return $this->type;
    }

    public function getProductCount(){
        return $this->productCount;
    }

    public function setProducts($productNames){
        $this->productNames = $productNames;
    }

    public function getProductNames(){
        return $this->productNames;
    }

    public function setDiscount($discount){
        $this->discount = $discount;
    }

    public function getDiscount(){
        return $this->discount;
    }
}