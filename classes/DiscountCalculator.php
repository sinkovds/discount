<?php

//класс калькулятор скидок
class DiscountCalculator{

    private $productSet;
    private $discountList;

    public function DiscountCalculator(ProductSet $productSet, DiscountManager $discountManager){
        $this->productSet = $productSet;
        $this->discountList = $discountManager->getDiscountList();
    }

    public function setProductSet(ProductSet $productSet){
        $this->productSet = $productSet;
    }

    public function getProductSet(){
        return $this->productSet;
    }


    public function setDiscountList($discountList){
        $this->discountList = $discountList;
    }

    public function getDiscountList(){
        return $this->discountList;
    }

    // метод применения скидок
    public function applyDiscount(){

        foreach($this->discountList as $discount){

            switch($discount->getType()){
                case 0:
                    while(true){
                        $subset = $this->productSet->getSubsetByName($discount->getProductNames());

                        if($subset === false)
                            break;

                        foreach($subset->getProducts() as $hash => $product){
                            $this->productSet->setDiscountedByHash($hash,true);
                            $this->productSet->setDiscountedPriceByHash($hash, $product->getPrice() * (1 - $discount->getDiscount()));
                        }

                    }

                    break;
                case 1:

                    $hashes = array();
                    while(true){
                        $subset = $this->productSet->getSubsetByCount($discount->getProductCount(), $discount->getExcludedProducts(), $hashes);

                        if($subset === false)
                            break;

                        $hashes = array_merge($hashes, array_keys($subset->getProducts()));

                        foreach($subset->getProducts() as $hash => $product){
                            $this->productSet->setDiscountedByHash($hash,true);
                            $this->productSet->setDiscountedPriceByHash($hash, $product->getPrice() * (1 - $discount->getDiscount()));
                        }

                    }
                    break;
            }


        }
    }

    public function getSums(){
        $sum = 0;
        $sumDiscount = 0;

        foreach($this->productSet->getProducts() as $product){
            $sum += $product->getPrice();
            $sumDiscount += $product->getDiscountPrice();
        }

        return array(
            'total' => $sum,
            'totalDiscount' => $sumDiscount
        );
    }
}