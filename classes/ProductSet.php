<?php

//класс множества продуктов
class ProductSet{

    protected $products = array();


    //добавление в массив продуктов по хэшу объекта
    function add(Product $product, $hash = null){
        if(!$hash)
            $this->products[spl_object_hash($product)] = $product;
        else
            $this->products[$hash] = $product;
    }
    function getProducts(){
        return $this->products;
    }

    function  getCount(){
        return count($this->products);
    }

    function getProductNames(){
        $names = array();
        foreach($this->products as $product){
            array_push($names, $product->getName());
        }
        return $names;
    }

    //метод создания подмножества продуктов по массиву названий
    public function getSubsetByName($names){
        $subSet = new ProductSet();

        foreach($names as $name){
            $hash = $this->getHashByProductName($name);
            if($hash === false)
                return false;

            $subSet->add($this->products[$hash]);
        }
        return $subSet;
    }

    //метод создания подмножества продуктов по количеству
    public function getSubsetByCount($count, $excludedProducts = array(), $excludedHash = array()){

        $subSet = new ProductSet();

        foreach($this->products as $hash => $product){

            if(in_array($product->getName(), $excludedProducts) || in_array($hash, $excludedHash) || $product->getIsDiscounted() === true)
                continue;

            $subSet->add($this->products[$hash], $hash);

            if($subSet->getCount() == $count)
                return $subSet;
        }

        return false;
    }

    public function setProductSetDiscounted(ProductSet $products){
        foreach($products as $hash =>$product){
            $this->products[$hash]->setIsDiscounted(true);
        }
    }

    public function setDiscountedByHash($hash, $isDiscounted){
        $this->products[$hash]->setIsDiscounted($isDiscounted);
    }

    public function setDiscountedPriceByHash($hash, $price){
        $this->products[$hash]->setDiscountPrice($price);
    }

    public function getHashByProductName($name){
        foreach($this->products as $hash => $product){
            if($product->getName() == $name && $product->getIsDiscounted() === false)
                return $hash;
        }
        return false;
    }

}