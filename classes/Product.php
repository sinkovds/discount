<?php

class Product{

    protected $price; //цена продукта
    protected $productName; //название продукта
    protected $isDiscounted; //признак что скидка уже применена к продукту
    protected $discountPrice; //цена со скидкой

    public function Product($productName, $price){
        $this->price = $price;
        $this->productName = $productName;
        $this->isDiscounted = false;
        $this->discountPrice = $price;
    }

    //статический метод создания продукта
    public static function get($productName, $price){

        return new Product($productName, $price);
    }

    public function getName(){
        return $this->productName;
    }

    public function getPrice(){
        return $this->price;
    }

    public function setPrice($price){
        $this->price = $price;
    }

    public function getDiscountPrice(){
        return $this->discountPrice;
    }

    public function setDiscountPrice($discountPrice){
        $this->discountPrice = $discountPrice;
    }

    public function setIsDiscounted($isDiscounted){
        $this->isDiscounted = $isDiscounted;
    }
    public function getIsDiscounted(){
        return $this->isDiscounted;
    }
}
