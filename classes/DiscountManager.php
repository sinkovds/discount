<?php

class DiscountManager{
    protected $discountList = array();

    public function add(Discount $discount){
        array_push($this->discountList, $discount);
    }
    public function getDiscountList(){
        return $this->discountList;
    }

}