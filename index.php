<?php

spl_autoload_register(function ($class) {
    include __DIR__.'/classes/' . $class . '.php';
});

//создание объекта множества продуктов
$productSet = new ProductSet();

//добавление продуктов
$productSet->add(Product::get('A',100));
$productSet->add(Product::get('B',400));
$productSet->add(Product::get('C',300));
$productSet->add(Product::get('D',140));
$productSet->add(Product::get('E',120));
$productSet->add(Product::get('F',130));
$productSet->add(Product::get('G',110));
$productSet->add(Product::get('H',410));
$productSet->add(Product::get('I',530));
$productSet->add(Product::get('J',200));
$productSet->add(Product::get('K',310));
$productSet->add(Product::get('L',340));
$productSet->add(Product::get('M',100));

// создание менеджера скидок
$discountManager = new DiscountManager();

//добавление скидок
$discountManager->add(Discount::create(array('A','B'), 0.1));
$discountManager->add(Discount::create(array('D','E'), 0.05));
$discountManager->add(Discount::create(array('E','F', 'G'), 0.05));
$discountManager->add(Discount::create(array('A','K'), 0.05));
$discountManager->add(Discount::create(array('A','L'), 0.05));
$discountManager->add(Discount::create(array('A','M'), 0.05));

$discountManager->add(Discount::createByCount(5, 0.2, array('A','C')));
$discountManager->add(Discount::createByCount(4, 0.1, array('A','C')));
$discountManager->add(Discount::createByCount(3, 0.05, array('A','C')));

//создвние объекта калькулятора
$calculator = new DiscountCalculator($productSet, $discountManager);

//метод применения скидок
$calculator->applyDiscount();

//рассчет общей стоимости, возвращается массив содержащий общую стоимость без скидок и со скидкой
$sum = $calculator->getSums();
var_dump($sum);